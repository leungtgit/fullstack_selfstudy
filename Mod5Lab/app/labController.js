app.controller('labController', [
    '$scope', 'registration',
    function ($scope, registration) {
        $scope.reset = reset;
        $scope.submit = submit;

        reset();

        function submit(model, form) {
            registration.submit(model).$promise
                .then(function (response){
                    alert('success. token: ' + response.token);
                    reset();
                    $scope.form.$setPristine();
                    $scope.form.$setUntouched();
                },
                function (response) {
                        alert('error');
                });
            // $scope.model = model;
            // alert('Submitted\n' + JSON.stringify(model));
        }

        function reset() {
            $scope.model = {email: "eve.holt@reqres.in"};
        }
    }
]);