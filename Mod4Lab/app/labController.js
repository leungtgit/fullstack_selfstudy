app.controller('labController', [
    '$scope','$timeout', '$q', '$http', 'gitHub',
    function ($scope, $timeout, $q, $http, gitHub) {
        $scope.model = {
            number: 0,
            result: 'Ready'};

        $scope.checkOddNumber = checkOddNumber;

        function checkOddNumber(input) {
            $scope.model.result = "Working. . .";
            checkOddNumberHandler(input).then(function(result) {
                $scope.model.result = 'Success: ' + result;
            }, function (result) {
                $scope.model.result = 'Error: ' + result;
            })
        }

        function checkOddNumberHandler(input) {
            var defer = $q.defer();

            $timeout(function() {
                if(isNumberOdd(input)) {
                    defer.resolve('Yes. The number is odd.');
                } else {
                    defer.reject('No. The number is not odd.');
                }
            }, 1000);

            return defer.promise;

        }

        function isNumberOdd(input) {
            return !isNaN(input) && input % 2 == 1;
        }

        $scope.getRepos = getRepos;

        function getRepos(inputOrg) {
            //var defer
            $scope.model.repos = gitHub.getAll({org: inputOrg});
        }

        $scope.loadDetail = loadDetail;

        function loadDetail(name, inputOrg) {
            $scope.model.detail = null;
            $scope.model.detail = gitHub.getDetail({ id: name, org: inputOrg })
                .$promise.then(
                    function(result) {
                        $scope.model.result = 'Success: ' + result;
                        $scope.model.detail = result;
                    }, function(result) {
                        $scope.model.result = 'Error: ' + result;
                    }
                );
        }
    }
]);